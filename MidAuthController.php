<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\UnauthorizedException;
use App\User;
use Sk\Mid\Exception\DeliveryException;
use Sk\Mid\Exception\InvalidNationalIdentityNumberException;
use Sk\Mid\Exception\InvalidPhoneNumberException;
use Sk\Mid\Exception\InvalidUserConfigurationException;
use Sk\Mid\Exception\MidInternalErrorException;
use Sk\Mid\Exception\MidSessionNotFoundException;
use Sk\Mid\Exception\MidSessionTimeoutException;
use Sk\Mid\Exception\MissingOrInvalidParameterException;
use Sk\Mid\Exception\NotMidClientException;
use Sk\Mid\Exception\PhoneNotAvailableException;
use Sk\Mid\Exception\UserCancellationException;
use Sk\Mid\MobileIdClient;
use Sk\Mid\Language\ENG;
use Sk\Mid\Rest\Dao\Request\AuthenticationRequest;
use Sk\Mid\Util\MidInputUtil;
use Sk\Mid\DisplayTextFormat;
use Sk\Mid\MobileIdAuthenticationHashToSign;
use Sk\Mid\HashType\HashType;

class MidAuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Request from client to get verification code and authentication hash (base64)
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    
    public function authenticationRequest(Request $request){
        
        $inputNationalIdentityNumber = $request->json('id_code');
        $inputPhoneNumber = $request->json('phone');
        
        try {
            $phoneNumber = MidInputUtil::getValidatedPhoneNumber($inputPhoneNumber);
            $nationalIdentityNumber = MidInputUtil::getValidatedNationalIdentityNumber($inputNationalIdentityNumber);
        }
        catch (InvalidPhoneNumberException $e) {
            die('The phone number you entered is invalid');
        }
        catch (InvalidNationalIdentityNumberException $e) {
            die('The national identity number you entered is invalid');
        }
        
        
        $authenticationHash = MobileIdAuthenticationHashToSign::generateRandomHashOfDefaultType();
        $verificationCode = $authenticationHash->calculateVerificationCode();
      
        
        return response()->json([
            'authenticationHash'=> $authenticationHash->getHashInBase64(),
            'verificationCode' => $verificationCode], 201);
   
    }
    
    /**
     * Request from client to initiate message delivery to user's phone. Id-code, phone number and authentication
     * hash (base64) as input and session-id as output
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    
    public function authenticate(Request $request){
        
        $inputNationalIdentityNumber = $request->get('id_code');
        $inputPhoneNumber = $request->get('phone');
        $authenticationHashBase64 = base64_decode($request->get('authenticationHash'));
        
        try {
            $phoneNumber = MidInputUtil::getValidatedPhoneNumber($inputPhoneNumber);
            $nationalIdentityNumber = MidInputUtil::getValidatedNationalIdentityNumber($inputNationalIdentityNumber);
        }
        catch (InvalidPhoneNumberException $e) {
            die('The phone number you entered is invalid');
        }
        catch (InvalidNationalIdentityNumberException $e) {
            die('The national identity number you entered is invalid');
        }
        
        $client = $this->mIdClient();
        
        $authenticationHash = MobileIdAuthenticationHashToSign::newBuilder()
        ->withHashInBase64($authenticationHashBase64)
        ->withHashType(HashType::SHA256)
        ->build();
        
        
        
        $request = AuthenticationRequest::newBuilder()
        ->withPhoneNumber($phoneNumber)
        ->withNationalIdentityNumber($nationalIdentityNumber)
        ->withHashToSign($authenticationHash)
        ->withLanguage(ENG::asType())
        ->withDisplayText("Log into self-service?")
        ->withDisplayTextFormat(DisplayTextFormat::GSM7)
        ->build();
                
        try {
            $response = $client->getMobileIdConnector()->initAuthentication($request);
        }
        catch (NotMidClientException $e) {
            return response()->json([
                'message'=> 'Te pole Mobiil-ID kasutaja või teie sertifikaadid on tühistatud. Palun võtke ühendust oma mobiilioperaatoriga.'], 400);
        }
        catch (UnauthorizedException $e) {
            return response()->json([
                'message'=> 'Mobiil-ID integratsiooniviga. Valed MID ligipääsuandmed'], 400);
        }
        catch (MissingOrInvalidParameterException $e) {
            return response()->json([
                'message'=> 'Probleem MID integratsiooniga'], 400);
        }
        catch (MidInternalErrorException $e) {
            return response()->json([
                'message'=> 'MID sisemine viga'], 400);
        }
        
        return response()->json([
            'sessionId'=> $response->getSessionId()], 201);
      }
      
      /**
       * Request from client to get authentication process status. Session-Id and authentication
       * hash (base64) as input.
       * @param Request $request
       * @return \Illuminate\Http\JsonResponse
       */
    
    public function authenticationStatus(Request $request){
        
        $authenticationHashBase64 = base64_decode($request->get('authenticationHash'));
        $sessionId = $request->get('sessionId');
        
        $authenticationHash = MobileIdAuthenticationHashToSign::newBuilder()
        ->withHashInBase64($authenticationHashBase64)
        ->withHashType(HashType::SHA256)
        ->build();
        
        
        $client = $this->mIdClient();
        
        $finalSessionStatus = $client
        ->getSessionStatusPoller()
        ->fetchFinalSessionStatus($sessionId);
        
        if ($finalSessionStatus->isComplete()){
 
            try {
                $authenticatedPerson = $client
                ->createMobileIdAuthentication($finalSessionStatus, $authenticationHash)
                ->getValidatedAuthenticationResult()
                ->getAuthenticationIdentity();
            }
            catch (UserCancellationException $e) {
                return response()->json([
                    'message'=> 'Te tühistasite operatsiooni oma telefonist'], 400);
            }
            catch (MidSessionTimeoutException $e) {
                return response()->json([
                    'message'=> 'Te ei sisestanud oma telefonis PIN koodi või esines sideviga'], 400);
            }
            catch (PhoneNotAvailableException $e) {
                return response()->json([
                    'message'=> 'Telefon on kättesaamatu. Palun veenduge, et teie telefon oleks mobiililevis'], 400);
            }
            catch (DeliveryException $e) {
                return response()->json([
                    'message'=> 'Sideviga. Ei saa ühendust telefoniga'], 400);
            }
            catch (InvalidUserConfigurationException $e) {
                return response()->json([
                    'message'=> 'Mobiil-ID konfiguratsioon teie SIM kaardil erineb teenusepakkuja omast. Palun võtke ühendust oma mobiilsideoperaatoriga'], 400);
            }
            catch (MidSessionNotFoundException | MissingOrInvalidParameterException | UnauthorizedException $e) {
                return response()->json([
                    'message'=> 'Kliendipoolne Mobiil-ID integratsiooniviga. Veateade' . $e->getCode()], 400);
            }
            catch (NotMidClientException $e) {
                return response()->json([
                    'message'=> 'Te pole Mobiil-ID kasutaja või teie sertifikaadid on tühistatud. Palun võtke ühendust oma mobiilioperaatoriga.'], 400);
            }
            catch (MidInternalErrorException $internalError) {
                return response()->json([
                    'message'=> 'Mobiil-ID teenuses esines viga.'], 400);
            }
            
            # step #9 - read out authenticated person details
            return response()->json([
                'success'=> 'Authentication complete',
                'firstName' => $authenticatedPerson->getGivenName(),
                'surname' => $authenticatedPerson->getSurName(),
                'idCode' => $authenticatedPerson->getIdentityCode(),
                'country' => $authenticatedPerson->getCountry()
            ], 201);
            
        }
        else {
            return response()->json([
                'inprogress'=> 'Authentication in progress'], 200);
        }
    }
    
    private function mIdClient() : MobileIdClient {
        return MobileIdClient::newBuilder()
        ->withRelyingPartyUUID(env('MID_RELYING_PARTY_UUID'))
        ->withRelyingPartyName(env('MID_RELYING_PARTY_NAME'))
        ->withHostUrl(env('MID_HOST_URL'))
        ->withLongPollingTimeoutSeconds(60)
        ->withPollingSleepTimeoutSeconds(2)
        ->build();
    }
    
}