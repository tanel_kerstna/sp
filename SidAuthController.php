<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sk\SmartId\Client;
use Sk\SmartId\Api\Data\AuthenticationHash;
use Sk\SmartId\Api\Data\CertificateLevelCode;
use Sk\SmartId\Exception\SmartIdException;
use Sk\SmartId\Api\AuthenticationResponseValidator;
use Sk\SmartId\Exception\UserRefusedException;
use Sk\SmartId\Exception\SessionTimeoutException;

class SidAuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Request for client to get verification code and authentiation hash in base64 format
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticationRequest(Request $request){
        $authenticationHash = AuthenticationHash::generate();
        
        return response()->json([
            'authenticationHash'=> base64_encode($authenticationHash->getHash()),
            'verificationCode' => $authenticationHash->calculateVerificationCode()], 201);
        
    }
    /**
     * Request for client to trigger authentication notification delivery to user's device. 
     * Authentication hash in base64 and ID code as input parameters. Session id as output
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    
    public function authenticate(Request $request){
        
        $inputNationalIdentityNumber = $request->json('id_code');

        $authenticationHashCode = base64_decode($request->json('authenticationHash'));
        $authenticationHash = AuthenticationHash::generate();
        $authenticationHash->setHash($authenticationHashCode);
        $client = $this->sIdClient();
        
        try
        {
            $sessionId = $client->authentication()
            ->createAuthentication()
            ->withNationalIdentityNumber( $inputNationalIdentityNumber ) // or with document number: ->withDocumentNumber( 'PNOEE-10101010005-Z1B2-Q' )
            ->withCountryCode("EE")
            ->withDisplayText('Logi sisse portaali')
            ->withAuthenticationHash( $authenticationHash )
            ->withCertificateLevel( CertificateLevelCode::QUALIFIED ) // Certificate level can either be "QUALIFIED" or "ADVANCED"
            ->startAuthenticationAndReturnSessionId();
        }
        catch (SmartIdException $e) {
            die($e);
        }
        
        return response()->json([
            'sessionId'=> $sessionId], 201);
        
    }
    
    /**
     * Request for client to check authentication status with previously provided session id and 
     * authentication hash in base64
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
        
    public function authenticationStatus(Request $request){
        $sessionId = $request->json('sessionId');
        $authenticationHashCode = base64_decode($request->json('authenticationHash'));
        $authenticationHash = AuthenticationHash::generate();
        $authenticationHash->setHash($authenticationHashCode);
        
        $client = $this->sIdClient();
        
        try
        {
            $authenticationResponse = $client->authentication()
                ->createSessionStatusFetcher()
                ->withSessionId( $sessionId )
                ->withAuthenticationHash( $authenticationHash )
                ->withSessionStatusResponseSocketTimeoutMs( 10000 )
                ->getAuthenticationResponse();
            
            
            if ( $authenticationResponse->isRunningState() ) {
                return response()->json([
                    'inprogress'=> 'Authentication in progress'], 200);
            }
            else {
                $authenticationResponseValidator = new AuthenticationResponseValidator();
                $authenticationResult = $authenticationResponseValidator->validate( $authenticationResponse );
                $isValidResult = $authenticationResult->isValid();
                $authenticationIdentity = $authenticationResult->getAuthenticationIdentity();

                if ($isValidResult){
                    return response()->json([
                        'success'=> 'Authentication complete',
                        'firstName' => $authenticationIdentity->getGivenName(),
                        'surname' => $authenticationIdentity->getSurName(),
                        'idCode'=>$authenticationIdentity->getIdentityCode()
                    ], 200);
                }
                else {
                    return response()->json([
                        'message'=> $authenticationResult->getErrors()], 400);
                }
            }

            
        }
        catch (UserRefusedException $e){
            return response()->json([
                'message'=> 'Te tühistasite operatsiooni oma telefonist'], 400);
        }
        catch (SessionTimeoutException $e){
            return response()->json([
                'message'=> 'Te ei sisestanud oma telefonis PIN koodi või esines sideviga'], 400);
        }
        catch (SmartIdException $e) {
            return response()->json([
                'message'=> 'Smart-ID internal error'.$e], 400);
        }
        
        
    }
    
    /**
     * Function to create Smart id client
     * @return Client
     */
    
    private function sIdClient() : Client {
        $client = new Client();
        return $client
            ->setRelyingPartyUUID(env('SID_RELYING_PARTY_UUID'))
            ->setRelyingPartyName(env('SID_RELYING_PARTY_NAME'))
            ->setHostUrl(env('SID_HOST_URL'));
    }
    
}